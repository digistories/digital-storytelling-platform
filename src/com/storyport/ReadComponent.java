package com.storyport;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.storyport.ui.components.EmptyTopicComponent;
import com.storyport.ui.components.ReadStoryComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Responsive;
import com.vaadin.ui.CssLayout;

public class ReadComponent extends CssLayout {

	private static final long serialVersionUID = 8177820964291895769L;

	private ReadStoryComponent comp;
	private EmptyTopicComponent emptyComponent;

	
	public ReadComponent(JDBCConnectionPool pool /*, String userId*/){
		this.setPrimaryStyleName("scrollable-layout");
		Responsive.makeResponsive(this);
	}
	
	private void setTopic(Topic topic, JDBCConnectionPool pool, String userId){
		
		if(comp != null)
			removeComponent(comp);
		if(emptyComponent != null)
			removeComponent(emptyComponent);
		
		comp = new ReadStoryComponent(pool, userId, topic);
		addComponent(comp);
	}
	
	private void setEmpty(Topic topic, JDBCConnectionPool pool, String userId){
		if(comp != null)
			removeComponent(comp);
		if(emptyComponent != null)
			removeComponent(emptyComponent);
		
		emptyComponent = new EmptyTopicComponent(pool, userId, topic);
		addComponent(emptyComponent);
	}
	
	public boolean tryToSetTopic(int topicId, JDBCConnectionPool pool, String userId){
		if(FrontendConnector.hasPermission(pool, topicId, userId, false)){
			Topic topic = FrontendConnector.getTopic(pool, topicId);
			if(topic.getContent() == null || topic.getContent().isEmpty()){  // THE TOPIC IS AVAILABLE, BUT EMPTY
				setEmpty(topic, pool, userId);
				return true;
			}
			setTopic(topic, pool, userId);
			
			return true;
		}
		return false;
	}

}
