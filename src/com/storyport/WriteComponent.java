package com.storyport;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;

public class WriteComponent extends CssLayout{
	
	private static final long serialVersionUID = -8305154513455855246L;
	private Label title, description;
	private Topic currentTopic;
	private TextArea storyField;
	
	private static final int TEXT_MIN_LENGTH = 10;
	private static final int TEXT_MAX_LENGTH = Integer.MAX_VALUE;
	
	public WriteComponent(JDBCConnectionPool pool, String userId){
		this.setPrimaryStyleName("topic-read-panel-wrapper");

		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->Page.getCurrent().getJavaScript().execute("window.history.back();"));
		
		title = new Label("");
		title.setPrimaryStyleName("header inline-block");
		
		
		Button submit = new Button("Submit");
		submit.setPrimaryStyleName("theme-button");
		
		description = new Label("");
		description.setPrimaryStyleName("topic-description");
		storyField = new TextArea("Your story:");

		CssLayout textPanel = new CssLayout(description, storyField, submit);
		textPanel.setStyleName("topic-read-panel");
		
		addComponents(back, title, textPanel);		
		
		submit.addClickListener(e -> {
			String story = storyField.getValue();
			String errorMessage = validateStory(story);
			if(errorMessage == null){
				FrontendConnector.addFragment(pool, userId, currentTopic.getId(), storyField.getValue());
				storyField.setValue("");
				Page.getCurrent().getJavaScript().execute("window.history.back();");
			}
			else{
				Notification.show(errorMessage, Notification.Type.HUMANIZED_MESSAGE);
			}
		});
	}
	
	/**
	 * @return null if the content is okay, otherwise message that is shown to the user
	 */
	private String validateStory(String story){
		if(story.length() < TEXT_MIN_LENGTH)
			return "Text must be longer than " + TEXT_MIN_LENGTH + " characters";
		else if(story.length() > TEXT_MAX_LENGTH)
			return "Text can't be longet than " + TEXT_MAX_LENGTH + " characters";
		else if(story.trim().length() == 0)
			return "Text must contain characters";
		else
			return null;
	}
	
	private void setTopic(Topic topic, JDBCConnectionPool pool, String userId){
		title.setValue(topic.getTitle());
		description.setValue(topic.getDescription());
		currentTopic = topic;
	}
	
	public boolean tryToSetTopic(int topicId, JDBCConnectionPool pool, String userId){
		if(userId != null && FrontendConnector.hasPermission(pool, topicId, userId, false)){
			Topic topic = FrontendConnector.getTopic(pool, topicId);
			setTopic(topic, pool, userId);
			return true;
		}
		return false;
	}
	
}
