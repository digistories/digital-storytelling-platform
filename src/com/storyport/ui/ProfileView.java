package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.backend.FrontendConnector;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class ProfileView extends CssLayout implements View {

	private Label title, karma, episodes, topics;
	private JDBCConnectionPool pool;
	private TopBarComponent topBar;
	
	public ProfileView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		title = new Label("Profile");
		topBar = new TopBarComponent(3);
		
		episodes = new Label("");
		topics = new Label("");
		karma = new Label("");
		CssLayout mainLayout = new CssLayout();
		mainLayout.setStyleName("main-layout");
		
		mainLayout.addComponents(title, karma, episodes, topics);
		addComponents(topBar, mainLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String id = (String)getSession().getAttribute("userid");
		if(id == null || id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("alltopics");
			return;
		}
		
		karma.setValue("Episode karma: " + FrontendConnector.getUsersKarma(pool, id));
		episodes.setValue("Topics created: " + FrontendConnector.getUsersTopicCount(pool, id));
		topics.setValue("Episodes written: " + FrontendConnector.getUsersFragmentCount(pool, id));

	}
}
