package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class PrivacyPolicyView extends CssLayout implements View {

	private CssLayout main, text;
	
	public PrivacyPolicyView() {
		setStyleName("front-page");
		addComponent( new TopBarComponent(0) );
		
		main = new CssLayout();
		main.setPrimaryStyleName("scrollable-layout");
		Responsive.makeResponsive(main);
		
		text = new CssLayout();
		text.setPrimaryStyleName("topic-read-panel-wrapper");
		
		this.addComponent(main);
		main.addComponent(text);
		
		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->UI.getCurrent().getNavigator().navigateTo(""));
		text.addComponent(back);
		
		Label title = new Label("Privacy Policy");
		title.setPrimaryStyleName("header inline-block");
		text.addComponent(title);
		Responsive.makeResponsive(title);

		CssLayout textPanel = new CssLayout();
		textPanel.setStyleName("topic-read-panel");
		addComponent(textPanel);
		Responsive.makeResponsive(textPanel);
		
		Label empty = new Label("By logging in with your Facebook account, you give us your Facebook user id. StoryPort stores this id and uses it to identify between different users. StoryPort will not store any other personal information.<br><br>Your Facebook user id is also used to obtain your full name from Facebook. If you create a new topic in StoryPort, your name is displayed as the topic creator and anyone who has access to read that topic can see it. If you write stories in StoryPort, your name is displayed as the writer of that story and anyone who has access to read the topic can see it. Currently anyone who logs into StoryPort with his/her Facebook account can read any topic or story.<br><br>Your data will not be shared with any third parties.<br><br>You can get your personal data removed by sending an e-mail. You must include your Facebook user id in your e-mail so we can find and remove your data.<br><br><b> Contact: </b><br> Ky�sti Puro<br>e-mail: kyosti_puro@hotmail.com");
		empty.setContentMode(ContentMode.HTML);
		textPanel.addComponent(empty);
		empty.setStyleName("topic-empty");
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		
	}

}
