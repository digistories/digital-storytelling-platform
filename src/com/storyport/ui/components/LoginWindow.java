package com.storyport.ui.components;

import com.storyport.auth.FbLoginButton;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Window;

public class LoginWindow extends Window {
	
	private FbLoginButton loginButton;
	
	public LoginWindow(){
		super();
		setModal(true);
		setResizable(false);
		setDraggable(false);
		setWidth("50%");
		setStyleName("login-window");
		center();
		
		loginButton = new FbLoginButton();
		
		CssLayout loginButtonWrapper = new CssLayout(loginButton);
		loginButtonWrapper.setStyleName("login-button-wrapper");
		setContent(loginButtonWrapper);
	}
	
	public void statusChangeCallback(){
		close();
	}
	
}
