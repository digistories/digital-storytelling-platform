package com.storyport.ui.components;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Responsive;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class TopicButton extends CssLayout {

	public TopicButton(Topic topic, JDBCConnectionPool pool){
		setPrimaryStyleName("topic-button");
		
		Label title = new Label(topic.getTitle());
		title.setPrimaryStyleName("topic-title");
		
		Label desc = new Label(topic.getDescription());
		desc.setPrimaryStyleName("topic-description");
		
		Label fragments = new Label("" + FrontendConnector.getTopicFragmentCount(pool, topic.getId()));
		fragments.setPrimaryStyleName("topic-fragments");
		
		addComponents(title, desc, fragments);

		addLayoutClickListener(e -> UI.getCurrent().getNavigator().navigateTo("topic/" + topic.getId()));
		
		Responsive.makeResponsive(this);
	}
}
