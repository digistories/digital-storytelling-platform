package com.storyport.ui.components;

import com.storyport.backend.AlgorithmConnector;
import com.storyport.backend.Fragment;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PopupView;

public class AllFragmentsComponent extends CssLayout {

	private static final int SHOW_LENGTH = 100;
	
	public AllFragmentsComponent(JDBCConnectionPool pool, String userId, Topic topic) {
		this.setPrimaryStyleName("topic-read-panel-wrapper");
		
		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->Page.getCurrent().getJavaScript().execute("window.history.back();"));
		this.addComponent(back);
		
		Label title = new Label(topic.getTitle());
		title.setPrimaryStyleName("header inline-block");
		this.addComponent(title);
		Responsive.makeResponsive(title);
		
		CssLayout textPanel = new CssLayout();
		textPanel.setStyleName("topic-read-panel");
		Responsive.makeResponsive(textPanel);
		
		Fragment[] fragments = AlgorithmConnector.getFragments(pool, topic.getId());
		for(Fragment f : fragments) {
			String partContent = (f.getContent().length() > SHOW_LENGTH + 3) ? f.getContent().substring(0, SHOW_LENGTH) + "..." : f.getContent();
					
			Component popupLarge = new FullFragmentComponent(f, userId, pool);
			popupLarge.setPrimaryStyleName("fragment-popup");
			
			PopupView popup = new PopupView(partContent, popupLarge);
			popup.setHideOnMouseOut(false);
			popup.setStyleName("fragment");

			popup.addPopupVisibilityListener(e -> {
				if(e.isPopupVisible())
					((FullFragmentComponent) popup.getContent().getPopupComponent()).onOpen();
			});
			
			textPanel.addComponent(popup);
		}
		
		this.addComponents(textPanel);
	}
}
