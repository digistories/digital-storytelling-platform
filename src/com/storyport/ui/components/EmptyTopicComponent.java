package com.storyport.ui.components;

import com.storyport.auth.AppAccessTokenHandler;
import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class EmptyTopicComponent extends CssLayout {

	public EmptyTopicComponent(JDBCConnectionPool pool, String userId, Topic topic) {
		this.setPrimaryStyleName("topic-read-panel-wrapper");
		Responsive.makeResponsive(this);
		
		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->Page.getCurrent().getJavaScript().execute("window.history.back();"));
		this.addComponent(back);
		
		Label title = new Label(topic.getTitle());
		title.setPrimaryStyleName("header inline-block");
		this.addComponent(title);
		Responsive.makeResponsive(title);
		
		
		Button writeToThisTopic = new Button("Write to this topic");
		writeToThisTopic.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("write/" + topic.getId()));
		writeToThisTopic.setPrimaryStyleName("theme-button right");
		this.addComponent(writeToThisTopic);
		Responsive.makeResponsive(writeToThisTopic);
		
		if(FrontendConnector.isAdmin(pool, userId)){
			Button deleteTopic = new Button("Remove Topic");
			deleteTopic.addClickListener(e ->FrontendConnector.deleteTopic(pool, topic.getId()));
			deleteTopic.setPrimaryStyleName("theme-button right");
			this.addComponent(deleteTopic);
		}
		
		CssLayout textPanel = new CssLayout();
		textPanel.setStyleName("topic-read-panel");
		addComponent(textPanel);
		Responsive.makeResponsive(textPanel);
		
		Label empty = new Label("This topic is empty. Be the first to write to this topic?");
		textPanel.addComponent(empty);
		empty.setStyleName("topic-empty");
		
		Label creator = new Label("Topic creator: " + AppAccessTokenHandler.getUserName(topic.getAuthorId()));
		textPanel.addComponent(creator);
		creator.setStyleName("topic-empty");
		
		Label desc = new Label("Topic Description: " + AppAccessTokenHandler.getUserName(topic.getDescription()));
		textPanel.addComponent(desc);
		desc.setStyleName("topic-empty");
	}
}
