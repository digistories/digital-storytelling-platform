package com.storyport.ui.components;

import java.util.ArrayList;

import com.storyport.auth.AppAccessTokenHandler;
import com.storyport.backend.FragmentPart;
import com.storyport.backend.FrontendConnector;
import com.storyport.backend.StoryFormat;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.UI;

/**
 * Component that shows the generated story and allows user to see the original fragments.
 * @author Pekka
 *
 */
public class ReadStoryComponent extends CssLayout{
	
	public ReadStoryComponent(JDBCConnectionPool pool, String userId, Topic topic) {
		this.setPrimaryStyleName("topic-read-panel-wrapper");
		Responsive.makeResponsive(this);
		
		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->Page.getCurrent().getJavaScript().execute("window.history.back();"));
		this.addComponent(back);
		
		Label title = new Label(topic.getTitle());
		title.setPrimaryStyleName("header inline-block");
		Responsive.makeResponsive(title);
		this.addComponent(title);
		
		
		Button writeToThisTopic = new Button("Write to this topic");
		writeToThisTopic.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("write/" + topic.getId()));
		writeToThisTopic.setPrimaryStyleName("theme-button right");
		Responsive.makeResponsive(writeToThisTopic);
		this.addComponent(writeToThisTopic);
		
		if(topic.getAuthorId().equals(userId) || FrontendConnector.isAdmin(pool, userId)){
			Button fullFragments = new Button("Read all fragments");
			fullFragments.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("fulltopic/" + topic.getId()));
			fullFragments.setPrimaryStyleName("theme-button right");
			this.addComponent(fullFragments);
		}
		if(FrontendConnector.isAdmin(pool, userId)){
			Button deleteTopic = new Button("Remove Topic");
			deleteTopic.addClickListener(e ->FrontendConnector.deleteTopic(pool, topic.getId()));
			deleteTopic.setPrimaryStyleName("theme-button right");
			this.addComponent(deleteTopic);
		}
		CssLayout textPanel = new CssLayout();
		textPanel.setStyleName("topic-read-panel");
		Responsive.makeResponsive(textPanel);
		
		textPanel.addComponent(new Label("Topic creator: " +
				AppAccessTokenHandler.getUserName(topic.getAuthorId()), ContentMode.HTML));
		textPanel.addComponent(new Label("Topic Description: " +
				AppAccessTokenHandler.getUserName(topic.getDescription()) + "</br></br>", ContentMode.HTML));
		
		ArrayList<FragmentPart> parts = StoryFormat.decodeStory(topic.getContent());
		
		for(FragmentPart fp : parts) {
			String partContent = fp.getContent();
			
//			Fragment originalFragment = FrontendConnector.getFragment(pool, fp.getFragmentId());
//			
//			Component popupLarge;
//			if(originalFragment != null){
//				popupLarge = new FullFragmentComponent(originalFragment, userId, pool);
//				popupLarge.setPrimaryStyleName("fragment-popup");
//			}
//			else{
//				popupLarge = new Label("This fragment has been removed. This story is being rebuild...");
//			}
			
			Component popupLarge = new FullFragmentComponent(fp.getFragmentId(), userId, pool);
			
			PopupView popup = new PopupView(partContent, popupLarge);
			popup.setHideOnMouseOut(false);
			popup.setStyleName("fragment");
			
			popup.addPopupVisibilityListener(e -> {
				if(e.isPopupVisible())
					((FullFragmentComponent) popup.getContent().getPopupComponent()).onOpen();
			});
			
			textPanel.addComponent(popup);
		}
		
		this.addComponents(textPanel);
	}
	

}
