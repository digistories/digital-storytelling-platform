package com.storyport.ui.components;

import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

public class MyTopicsMenuComponent extends CssLayout{

	private Button createdByMe, writtenByMe;
	
	public MyTopicsMenuComponent(int activeID){
		this.setPrimaryStyleName("my-topics-menu");
		

	
		createdByMe = new Button("Created by me");
		writtenByMe = new Button("Written by me");
		createdByMe.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("created"));
		writtenByMe.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("written"));
		
		if (activeID == 1) {
			createdByMe.setPrimaryStyleName("my-topics-menu-button-active");
			writtenByMe.setPrimaryStyleName("my-topics-menu-button");
		}
		else {
			createdByMe.setPrimaryStyleName("my-topics-menu-button");
			writtenByMe.setPrimaryStyleName("my-topics-menu-button-active");
		}
		
		addComponents(createdByMe, writtenByMe);
	}
}
