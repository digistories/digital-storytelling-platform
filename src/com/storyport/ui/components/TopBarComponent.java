package com.storyport.ui.components;

import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

public class TopBarComponent extends CssLayout {
	
	private Button mainView, logIn, signUp;
	private Button allTopics, myTopics, logout;
	//private Button profile;
	
	public TopBarComponent(int activeID){
		setStyleName("top-bar");
		
		mainView = new Button("StoryPort", new ThemeResource("logoSmall.png"));
		mainView.setPrimaryStyleName("logo-nav-button");
		mainView.addClickListener(e -> changeViewClicked(""));
		Responsive.makeResponsive(mainView);
		addComponent(mainView);
		
		if(UI.getCurrent().getSession().getAttribute("userid") == null){
			logIn = new Button("Log In");
			logIn.setPrimaryStyleName("right-nav-button");
			signUp = new Button("Sign Up");
			signUp.setPrimaryStyleName("right-nav-button");
			signUp.addStyleName("highlighted-nav-button");
			
					
			logIn.addClickListener(e -> logInButtonClicked());
			signUp.addClickListener(e -> logInButtonClicked());
			
			addComponents(signUp, logIn);
		}
		else{
			allTopics = new Button("All Topics");
			allTopics.setPrimaryStyleName("right-nav-button2");
			myTopics = new Button("My Topics");
			myTopics.setPrimaryStyleName("right-nav-button2");
//			profile = new Button("Profile");
//			profile.setPrimaryStyleName("right-nav-button2");
			logout = new Button("Log out");
			logout.setPrimaryStyleName("right-nav-button2");
			
			if (activeID == 1)
				allTopics.addStyleName("active-nav-button");
			else if (activeID == 2)
				myTopics.addStyleName("active-nav-button");
//			else if (activeID == 3)
//				profile.addStyleName("active-nav-button");
			
			allTopics.addClickListener(e -> changeViewClicked("alltopics"));
			myTopics.addClickListener(e -> changeViewClicked("written"));
//			profile.addClickListener(e -> changeViewClicked("profile"));

			logout.addClickListener(e -> logoutButtonClicked());
			
			//addComponents(logout, profile, myTopics, allTopics);
			addComponents(allTopics, myTopics, logout);
		}
		
		Responsive.makeResponsive(this);
	}
	
	private void changeViewClicked(String viewName){
		UI.getCurrent().getNavigator().navigateTo(viewName);
	}
	
	private void logInButtonClicked(){
		UI.getCurrent().addWindow(new LoginWindow());
	}
	
	private void logoutButtonClicked(){
		getSession().setAttribute("userid", null);
    	Page.getCurrent().reload();
    	UI.getCurrent().getNavigator().navigateTo("");
	}
}
