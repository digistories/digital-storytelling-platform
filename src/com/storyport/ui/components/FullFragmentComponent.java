package com.storyport.ui.components;

import com.storyport.auth.AppAccessTokenHandler;
import com.storyport.backend.Fragment;
import com.storyport.backend.FrontendConnector;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;

public class FullFragmentComponent extends CssLayout{
	
	private int fragmentId;
	private String userId;
	private JDBCConnectionPool pool;
	
	private Fragment fragment;
	
	private boolean initialized = false;
	
	public FullFragmentComponent(int fragmentId, String userId, JDBCConnectionPool pool){
		this.fragmentId = fragmentId;
		this.userId = userId;
		this.pool = pool;
		this.setPrimaryStyleName("fragment-popup");
	}
	
	public FullFragmentComponent(Fragment fragment, String userId, JDBCConnectionPool pool){
		this(fragment.getId(), userId, pool);
		this.fragment = fragment;
	}
	
	private void initialize() {
		initialized = true;
		
		if(fragment == null)
			fragment = FrontendConnector.getFragment(pool, fragmentId);
		
		if(fragment == null){
			addComponent(new Label("This fragment has been removed. This story is being rebuild..."));
			return;
		}
		
		this.addComponent(new Label(fragment.getContent()));
		
		Label writtenByLabel = new Label("Written by: " + AppAccessTokenHandler.getUserName(fragment.getUserId()));
		addComponent(writtenByLabel);
		
		if(userId != null){
			
			if(fragment.getScore() > 0)
				this.addComponent(new Label("<font color='green'> +" + fragment.getScore() + "</font>", ContentMode.HTML));
			else if(fragment.getScore() < 0)
				this.addComponent(new Label("<font color='red'>" + fragment.getScore() + "</font>", ContentMode.HTML));
			else
				this.addComponent(new Label("<font color='grey'>" + fragment.getScore() + "</font>", ContentMode.HTML));	
			
			
			Button downVote = new Button("-1");
			Button upVote = new Button("+1");
						
			this.addComponent(downVote);
			this.addComponent(upVote);
			
			upVote.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					if(!FrontendConnector.tryToVote(pool, fragment.getId(), userId, true)){
						Notification.show("You have already voted on this topic", Notification.Type.HUMANIZED_MESSAGE);
					}
				}
			});

			downVote.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					if(!FrontendConnector.tryToVote(pool, fragment.getId(), userId, false)){
						Notification.show("You have already voted on this topic", Notification.Type.HUMANIZED_MESSAGE);
					}
				}
			});
			
			if(FrontendConnector.isAdmin(pool, userId)){
				Button remove = new Button("Remove");
				this.addComponent(remove);
				remove.addClickListener(e->FrontendConnector.deleteFragment(pool, fragment));
			}
		}
	}
	
	/**
	 * Call this when opening the popup
	 */
	public void onOpen(){
		if(!initialized){
			initialize();
		}
	}
}
