package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.ReadComponent;
import com.storyport.backend.FrontendConnector;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class ReadView extends CssLayout implements View {

	private ReadComponent readComponent;
	private Label noAccess;
	private TopBarComponent topBar;
	private JDBCConnectionPool pool;
	
	public ReadView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		topBar = new TopBarComponent(1);
		
		readComponent = new ReadComponent(pool);
		
		noAccess = new Label("You do not have permission for this topic or the topic id is invalid");

		addComponents(topBar);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String userId = (String)getSession().getAttribute("userid");
		if(userId == null || userId.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		
		String uriFragment = event.getParameters();
		if (uriFragment!= null && !uriFragment.isEmpty()) {
			try{
				int id = Integer.parseInt(uriFragment);
				if(readComponent.tryToSetTopic(id, pool, userId)){
					openReader(canWriteToTopic(userId, id));
				}
				else{
					noPermission();
				}
			}
			catch(NumberFormatException nfe){
				noPermission();
			}
		}
		else{
			noPermission();
		}
	}
	
	public void openReader(boolean canWrite){
		addComponent(readComponent);
		
		removeComponent(noAccess);
	}
	
	public void noPermission(){
		removeComponent(readComponent);
		
		addComponent(noAccess);
	}
	
	private boolean canWriteToTopic(String userId, int topicId){
		return userId != null && FrontendConnector.hasPermission(pool, topicId, userId, true);
	}

}
