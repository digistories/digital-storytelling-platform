package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class MainView extends CssLayout implements View {

	private Label title;
	private TopBarComponent topBar;
	
	private Button allTopics, myTopics;
	
	public MainView(){
		setStyleName("front-page");
		

		
		title = new Label("Topics");
		title.setStyleName("header");
		topBar = new TopBarComponent(0);
		
		allTopics = new Button("All Topics");
		allTopics.setPrimaryStyleName("button-big");
		myTopics = new Button("My Topics");
		myTopics.setPrimaryStyleName("button-big");
		
		allTopics.addClickListener(e -> changeViewClicked("alltopics"));
		myTopics.addClickListener(e -> changeViewClicked("written"));

		
		CssLayout mainLayout = new CssLayout(title, allTopics, myTopics);
		mainLayout.setStyleName("main-layout");
		
		addComponents(topBar, mainLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String id = (String)getSession().getAttribute("userid");
		if(id == null || id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
		}
	}
	
	private void changeViewClicked(String viewName){
		UI.getCurrent().getNavigator().navigateTo(viewName);
	}
}
