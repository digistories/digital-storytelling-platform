package com.storyport.ui;
/**
 * @author Ky�sti
 */
import java.util.ArrayList;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.storyport.ui.components.TopBarComponent;
import com.storyport.ui.components.TopicButton;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class AllTopicsView extends CssLayout implements View {

	private Label title;
	private TopBarComponent topBar;
	private ArrayList<TopicButton> topicButtons;
	private Button createTopic;
	
	private JDBCConnectionPool pool;
	
	private CssLayout mainLayout;
	
	public AllTopicsView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		topBar = new TopBarComponent(1);
		title = new Label("All Topics");
		title.setPrimaryStyleName("header inline-block");
		
		createTopic = new Button("Create Topic");
		createTopic.setPrimaryStyleName("new-topic-button");
		createTopic.setIcon(new ThemeResource("plusSmall.png"));
		createTopic.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("newtopic"));
		Responsive.makeResponsive(createTopic);
		
		topicButtons = new ArrayList<TopicButton>();
		
		mainLayout = new CssLayout();
		mainLayout.setStyleName("topic-list-panel");
		
		CssLayout wrapperLayout = new CssLayout(title, createTopic, mainLayout);
		wrapperLayout.setStyleName("scrollable-layout");
		Responsive.makeResponsive(wrapperLayout);
		
		addComponents(topBar, wrapperLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String id = (String)getSession().getAttribute("userid");
		if(id == null || id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		
		for (TopicButton b : topicButtons) {
			mainLayout.removeComponent(b);
		}
		topicButtons.clear();
		
		Topic[] availableTopics = FrontendConnector.getAvailableTopics(pool, (String)getSession().getAttribute("userid"), false);
		for(Topic t : availableTopics){
			TopicButton b = new TopicButton(t, pool);
			mainLayout.addComponent(b);
			topicButtons.add(b);
		}
	}

}
