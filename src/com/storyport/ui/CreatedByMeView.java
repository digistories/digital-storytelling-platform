package com.storyport.ui;
/**
 * @author Ky�sti
 */
import java.util.ArrayList;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.storyport.ui.components.MyTopicsMenuComponent;
import com.storyport.ui.components.TopBarComponent;
import com.storyport.ui.components.TopicButton;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class CreatedByMeView extends CssLayout implements View {

	private Label title;
	private TopBarComponent topBar;
	private JDBCConnectionPool pool;
	
	private MyTopicsMenuComponent myTopics;
	
	private ArrayList<TopicButton> topicButtons;
	
	private CssLayout mainLayout;
	
	public CreatedByMeView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		Button createTopic = new Button("Create Topic");
		createTopic.setPrimaryStyleName("new-topic-button");
		createTopic.setIcon(new ThemeResource("plusSmall.png"));
		createTopic.addClickListener(e -> UI.getCurrent().getNavigator().navigateTo("newtopic"));
		Responsive.makeResponsive(createTopic);
		
		topBar = new TopBarComponent(2);
		title = new Label("My Topics");
		title.setPrimaryStyleName("header inline-block");
		
		myTopics = new MyTopicsMenuComponent(1);
		
		topicButtons = new ArrayList<TopicButton>();
		
		mainLayout = new CssLayout();
		mainLayout.setStyleName("topic-list-panel");
		Responsive.makeResponsive(mainLayout);
		
		CssLayout wrapperLayout = new CssLayout(title, createTopic, myTopics, mainLayout);
		wrapperLayout.setStyleName("scrollable-layout");
		Responsive.makeResponsive(wrapperLayout);
		
		addComponents(topBar, wrapperLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String id = (String)getSession().getAttribute("userid");
		if(id == null || id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		showTopicsCreated();
	}
	
	private void showTopicsCreated(){
		
		for (TopicButton b : topicButtons) {
			mainLayout.removeComponent(b);
		}
		topicButtons.clear();
		
		Topic[] availableTopics = FrontendConnector.getUsersTopics(pool, (String)getSession().getAttribute("userid"));
		for(Topic t : availableTopics){
			TopicButton b = new TopicButton(t, pool);
			mainLayout.addComponent(b);
			topicButtons.add(b);
		}
	}

}
