package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.ui.components.LoginWindow;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

public class FrontPage extends CssLayout implements View{
	
	private Label header;
	private Label subHeader;
	private TextField searchField;
	private Button getStartedButton;
	private TopBarComponent topBar;
	
	public FrontPage(){
		setStyleName("front-page");
		
		CssLayout mainLayout = new CssLayout();
		mainLayout.setStyleName("main-layout");
		Responsive.makeResponsive(mainLayout);
		
		topBar = new TopBarComponent(0);
		
		header = new Label("Gather stories.");
		header.setStyleName("header");
		subHeader = new Label("StoryPort is a powerful tool for reporters to gather stories from people for their articles.");
		subHeader.setStyleName("subheader");
		
		
		searchField = new TextField();
		searchField.setStyleName("search-field");
		searchField.setEnabled(false);
		Responsive.makeResponsive(searchField);
		searchField.setInputPrompt("Search for stories...");
		
		getStartedButton = new Button("Get Started, Free!");
		getStartedButton.setPrimaryStyleName("theme-button");
		Responsive.makeResponsive(getStartedButton);
		
		CssLayout searchLayout = new CssLayout(searchField, getStartedButton);
		searchLayout.setStyleName("search-layout");
		Responsive.makeResponsive(searchLayout);
		
		Link privacy = new Link("Privacy Policy", new ExternalResource("#!privacy") );
		
		mainLayout.addComponents(header, subHeader, searchLayout, privacy);
		addComponents(topBar, mainLayout);
		
		getStartedButton.addClickListener(e -> getStartedButtonClicked());
	}
	
	private void getStartedButtonClicked(){
		UI.getCurrent().addWindow(new LoginWindow());
	}

	@Override
	public void enter(ViewChangeEvent event) {
		String id = (String)getSession().getAttribute("userid");
		if(id != null && !id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("alltopics");
		}
	}
}
