package com.storyport.ui;

/**
 * @author Ky�sti
 */
import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.storyport.ui.components.AllFragmentsComponent;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

public class AllFragmentsView extends CssLayout implements View {
	
	private JDBCConnectionPool pool;
	private TopBarComponent topBar;
	private AllFragmentsComponent allFrags;
	private CssLayout readLayout;
	
	public AllFragmentsView(JDBCConnectionPool pool){
		setStyleName("front-page");
		
		topBar = new TopBarComponent(2);
		
		this.pool = pool;
		addComponent(topBar);
		
		readLayout = new CssLayout();
		readLayout.setPrimaryStyleName("scrollable-layout");
		Responsive.makeResponsive(readLayout);
		addComponent(readLayout);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		String userId = (String)getSession().getAttribute("userid");
		if(userId == null || userId.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		
		String uriFragment = event.getParameters();
		if (uriFragment!= null && !uriFragment.isEmpty()) {
			try{
				int id = Integer.parseInt(uriFragment);
				Topic t = FrontendConnector.getTopic(pool, id);
				if(!t.getAuthorId().equals(userId) && !FrontendConnector.isAdmin(pool, userId)){
					UI.getCurrent().getNavigator().navigateTo("");
					return;
				}
				else{
					if(allFrags != null){
						readLayout.removeComponent(allFrags);
					}
					allFrags = new AllFragmentsComponent(pool, userId, t);
					readLayout.addComponent(allFrags);
				}
			}
			catch(NumberFormatException nfe){
				UI.getCurrent().getNavigator().navigateTo("");
				return;
			}
		}
		else{
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		
		
	}
	
	
}
