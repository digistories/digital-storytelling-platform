package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.TopicComponent;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class NewTopicView extends CssLayout implements View {

	private TopicComponent topicComponent;
	private Label title;
	private JDBCConnectionPool pool;
	private TopBarComponent topBar;
	private CssLayout mainLayout;
	
	public NewTopicView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		title = new Label("Create Topic");
		title.setPrimaryStyleName("header inline-block");
		Responsive.makeResponsive(title);
		
		topBar = new TopBarComponent(2);
		
		Button back = new Button();
		back.setPrimaryStyleName("back-button");
		back.addClickListener(e->Page.getCurrent().getJavaScript().execute("window.history.back();"));
		
		mainLayout = new CssLayout(back, title);
		mainLayout.setStyleName("scrollable-layout");
		Responsive.makeResponsive(mainLayout);
		
		addComponents(topBar, mainLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		if(topicComponent != null)
			mainLayout.removeComponent(topicComponent);
		
		String id = (String)getSession().getAttribute("userid");
		if(id == null || id.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("alltopics");
			return;
		}
		
		topicComponent = new TopicComponent(pool, id);
		mainLayout.addComponent(topicComponent);
	}

}
