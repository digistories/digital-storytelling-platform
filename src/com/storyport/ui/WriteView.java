package com.storyport.ui;
/**
 * @author Ky�sti
 */
import com.storyport.WriteComponent;
import com.storyport.ui.components.TopBarComponent;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Responsive;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class WriteView extends CssLayout implements View {

	private WriteComponent writeComponent;
	private Label noAccess;
	private TopBarComponent topBar;
	private JDBCConnectionPool pool;
	
	private CssLayout mainLayout;
	
	public WriteView(JDBCConnectionPool pool){
		this.pool = pool;
		setStyleName("front-page");
		
		topBar = new TopBarComponent(1);
		writeComponent = new WriteComponent(pool, (String)UI.getCurrent().getSession().getAttribute("userid"));
		noAccess = new Label("You do not have permission for this topic or the topic id is invalid");
		
		mainLayout = new CssLayout();
		mainLayout.setStyleName("scrollable-layout");
		Responsive.makeResponsive(mainLayout);
		
		
		addComponents(topBar, mainLayout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		String userId = (String)getSession().getAttribute("userid");
		if(userId == null || userId.isEmpty()){
			UI.getCurrent().getNavigator().navigateTo("");
			return;
		}
		
		String uriFragment = event.getParameters();
		if (uriFragment!= null && !uriFragment.isEmpty()) {
			try{
				int id = Integer.parseInt(uriFragment);
				if(writeComponent.tryToSetTopic(id, pool, userId)){
					openReader();
				}
				else{
					noPermission();
				}
			}
			catch(NumberFormatException nfe){
				noPermission();
			}
		}
		else{
			noPermission();
		}
	}
	
	public void openReader(){
		mainLayout.addComponent(writeComponent);
		mainLayout.removeComponent(noAccess);
	}
	
	public void noPermission(){
		mainLayout.removeComponent(writeComponent);
		mainLayout.addComponent(noAccess);
	}
}
