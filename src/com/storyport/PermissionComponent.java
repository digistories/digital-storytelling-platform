package com.storyport;

import com.storyport.backend.FrontendConnector;
import com.storyport.backend.Topic;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class PermissionComponent extends VerticalLayout{

	public PermissionComponent(JDBCConnectionPool pool, String userId) {
		
		ComboBox select = new ComboBox("Subjects");
		select.setNullSelectionAllowed(false);
		
		TextField permissionId = new TextField("User:");
		Button apply = new Button("Apply");
		
		addComponent(select);
		addComponent(permissionId);
		addComponent(apply);
		
		Topic[] topics = FrontendConnector.getUsersTopics(pool, userId);
		for(int i = 0; i < topics.length; i++){
			select.addItem(topics[i]);
		}
		
		apply.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				FrontendConnector.addPermission(pool, ((Topic)select.getValue()).getId(), permissionId.getValue());
				Page.getCurrent().reload();
			}
		});
	}
}
