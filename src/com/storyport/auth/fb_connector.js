/**
 * 
 */

com_example_auth_FbLoginButton = function(){
	var that = this;

    FB.init({
      appId      : '714692368686353',
      xfbml      : true,
      version    : 'v2.8'
    });
	
	
    // Listener for any auth related changes: login, logout, session refresh
	FB.Event.subscribe('auth.authResponseChange', function(response) {
		var userId = null;
		var accessToken = null;
		if(response.status === 'connected'){
			userId = response.authResponse.userID;
			accessToken = response.authResponse.accessToken;
		}
		that.statusChangeCallback(userId, accessToken); //server-side callback
	});
	
	
	// setting attributes for the button
	// descriptions https://developers.facebook.com/docs/facebook-login/web/login-button
	var element = document.getElementById("fb-login-button");
	element.setAttribute("data-max-rows", "1");
	element.setAttribute("data-size", "large");
	element.setAttribute("data-show-faces", "true");
	element.setAttribute("data-auto-logout-link", "true");
	
	
}
