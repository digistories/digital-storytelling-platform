package com.storyport.auth;

import com.storyport.ui.components.LoginWindow;
import com.vaadin.annotations.JavaScript;
import com.vaadin.server.Page;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.Notification;

import elemental.json.JsonArray;

// First load facebook's javascript API, then own scripts that depend on it
@JavaScript({ "https://connect.facebook.net/en_US/all.js", "fb_connector.js" })
public class FbLoginButton extends AbstractJavaScriptComponent {
	
	private static final long serialVersionUID = 428551224143075539L;

	private LoginWindow parentComponent;
	
	public FbLoginButton() {
		this.setStyleName("fb-login-button");
		this.setId("fb-login-button");
		
		this.addFunction("statusChangeCallback", args -> statusChangeCallback(args));
	}
	
	public FbLoginButton(LoginWindow parentComponent){
		this();
		this.parentComponent = parentComponent;
	}
	
	/**
	 * Callback from the client-side when fb-authentication changes
	 */
	private void statusChangeCallback(JsonArray args) {
		String userId = args.get(0).asString();
		String accessToken = args.get(1).asString();
		
		if(AppAccessTokenHandler.isTokenValid(userId, accessToken)){
			getSession().setAttribute("userid", userId);
			Page.getCurrent().reload();
		}
		else{
			Notification.show("Login failed.");
		}
		

		if(parentComponent != null){
			parentComponent.statusChangeCallback();
		}
	}
	
}
