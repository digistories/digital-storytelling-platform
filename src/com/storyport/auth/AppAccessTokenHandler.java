package com.storyport.auth;
/**
 * @author Ky�sti
 */
import java.util.Arrays;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.DebugTokenInfo;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.json.JsonObject;

public class AppAccessTokenHandler {
	
	private static final String APP_ID = ""; //REDACTED
	private static final String APP_SECRET = ""; //REDACTED
	
	private static FacebookClient.AccessToken appAccessToken;
	private static DefaultFacebookClient fbClient;
	
	private static void checkToken(){
		if(appAccessToken == null){
			appAccessToken = new DefaultFacebookClient(Version.VERSION_2_8).obtainAppAccessToken(APP_ID, APP_SECRET);
			fbClient = new DefaultFacebookClient(appAccessToken.getAccessToken(), Version.VERSION_2_8);
		}
		else if(fbClient == null){
			fbClient = new DefaultFacebookClient(appAccessToken.getAccessToken(), Version.VERSION_2_8);
		}
	}
	
	public static boolean isTokenValid(String userId, String tokenId){
		checkToken();
		
		DebugTokenInfo dti = fbClient.debugToken(tokenId);
		return dti.isValid() && dti.getUserId().equals(userId);
	}
	
	public static String getUserName(String userId){
		checkToken();
		try{
			JsonObject fetchObjectsResults =
					fbClient.fetchObjects(Arrays.asList(userId), 
					           JsonObject.class, Parameter.with("fields","name,id"));
			
			return ((JsonObject)fetchObjectsResults.get(userId)).getString("name");
		}
		catch(FacebookOAuthException foae){
			return userId;
		}
	}
}
