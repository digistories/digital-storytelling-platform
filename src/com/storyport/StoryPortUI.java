package com.storyport;

/**
 * @author Ky�sti
 */
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

import com.storyport.ui.AllFragmentsView;
import com.storyport.ui.AllTopicsView;
import com.storyport.ui.CreatedByMeView;
import com.storyport.ui.FrontPage;
import com.storyport.ui.NewTopicView;
import com.storyport.ui.PrivacyPolicyView;
import com.storyport.ui.ProfileView;
import com.storyport.ui.ReadView;
import com.storyport.ui.WriteView;
import com.storyport.ui.WrittenByMeView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("digitalstorytellingplatform")
@Viewport("width=device-width, initial-scale=1.0")

public class StoryPortUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = StoryPortUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	private static transient JDBCConnectionPool pool;

	private Navigator navigator;
	
	@Override
	protected void init(VaadinRequest request) {
		try {
			pool = new SimpleJDBCConnectionPool("org.sqlite.JDBC", "jdbc:sqlite:/storyport/StoryPort.db", "", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		navigator = new Navigator(this, this);
		
		Page.getCurrent().setTitle("StoryPort");
		
		FrontPage fp = new FrontPage();
		
		navigator.addView("", fp);
		navigator.addView("topic", new ReadView(pool));
		navigator.addView("write", new WriteView(pool));
		navigator.addView("alltopics", new AllTopicsView(pool));
		navigator.addView("profile", new ProfileView(pool));
		navigator.addView("newtopic", new NewTopicView(pool));
		navigator.addView("written", new WrittenByMeView(pool));
		navigator.addView("created", new CreatedByMeView(pool));
		navigator.addView("fulltopic", new AllFragmentsView(pool));
		navigator.addView("privacy", new PrivacyPolicyView());
		
		navigator.setErrorView(fp);
	}

}