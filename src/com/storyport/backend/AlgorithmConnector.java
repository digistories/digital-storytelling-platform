package com.storyport.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;

/**
 * Algorithm to database connector
 * @author Ky�sti
 *
 */
public class AlgorithmConnector {
	
	public static Fragment[] getFragments(JDBCConnectionPool pool, int id){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT id,content,topic_id,user_id,draft,score FROM Episode WHERE topic_id=?");
	        statement.setInt(1, id );
	        
	        resultSet = statement.executeQuery();
	        while(resultSet.next()){
	        	fragmentArray.add(new Fragment(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4),resultSet.getInt(5)==1,resultSet.getInt(6) ));
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		Fragment[] fragments = new Fragment[fragmentArray.size()];
		fragmentArray.toArray(fragments);
		
    	return fragments;
	}

	public static int getStatus(JDBCConnectionPool pool, int topicId){
		
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		int result = -1;
		try{
	        connection = pool.reserveConnection();
	
	        statement = connection.prepareStatement("SELECT status FROM Topic WHERE id=?");
	        statement.setInt(1, topicId);
	        
	        resultSet = statement.executeQuery();
	        
	        if(!resultSet.isClosed()){
	        	result = resultSet.getInt(1);
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public static void setStatus(JDBCConnectionPool pool, int topicId, int state){
		
		Connection connection = null;  
		PreparedStatement statement = null;
		
		try{
	        connection = pool.reserveConnection();
	
	        statement = connection.prepareStatement("UPDATE Topic SET status=? WHERE id=?");
	        statement.setInt(1, state);
	        statement.setInt(2, topicId);
	        
	        statement.executeUpdate();
	        connection.commit();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public static void setContentAndStatus(JDBCConnectionPool pool, int id, String content, int status){
		Connection connection = null;  
		PreparedStatement statement = null;
		boolean nullContent = content==null || content.length() == 0;
		if(nullContent){
			try{
		        connection = pool.reserveConnection();
		
		        statement = connection.prepareStatement("UPDATE Topic SET content=null,status=? WHERE id=?");
		        statement.setInt(1, status);
		        statement.setInt(2, id);
		        
		        statement.executeUpdate();
		        connection.commit();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				try{
					statement.close();
		        	connection.close();
		        	pool.releaseConnection(connection);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		else{
			try{
		        connection = pool.reserveConnection();
		
		        statement = connection.prepareStatement("UPDATE Topic SET content=?,status=? WHERE id=?");
		        statement.setString(1, content);
		        statement.setInt(2, status);
		        statement.setInt(3, id);
		        
		        statement.executeUpdate();
		        connection.commit();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				try{
					statement.close();
		        	connection.close();
		        	pool.releaseConnection(connection);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
