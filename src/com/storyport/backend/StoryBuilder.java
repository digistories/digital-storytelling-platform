package com.storyport.backend;
/**
 * @author Ky�sti
 * Handles building the summary
 */
import com.storyport.algorithm.Summarizer;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;

public class StoryBuilder {
	
	/*
	 * This is the only method that changes the "Status"-value in the database.
	 * List of status values and their meanings.
	 * 
	 * 0: The story is not being updated
	 * 1: The story is being updated
	 * 2: The story is being updated and another update is queued
	 * 
	 * Calling this method changes the value:
	 * 0 -> 1 (Starts assembly)
	 * 1 -> 2
	 * 2 -> 2
	 * 
	 * When the story is formed:
	 * 1->0
	 * 2->1
	 */
	public static void askForBuild(JDBCConnectionPool pool, int topicId){
		int state = AlgorithmConnector.getStatus(pool, topicId);
		if(state == 0){
			assembleStory(pool,  topicId);
			AlgorithmConnector.setStatus(pool, topicId, 1);
		}
		else if(state == 1){
			AlgorithmConnector.setStatus(pool, topicId, 2);
		}
	}

	private static void assembleStory(JDBCConnectionPool pool, int topic) {
		SequenceAssembler sa = new SequenceAssembler(topic, pool);
		sa.start();
	}
	
	private static class SequenceAssembler extends Thread{
		
		private Fragment[] inputs;
	    
	    private String outputText;
	    private JDBCConnectionPool pool;
	    private int topicId;
	            
	    public SequenceAssembler(int topicId, JDBCConnectionPool pool)
	    {
	    	this.topicId = topicId;
	    	this.pool = pool;
	    }
	    
	    @Override
	    public void run(){ 
			inputs = AlgorithmConnector.getFragments(pool, topicId);
			
			MergingAlgorithm summarizer = new Summarizer();
			outputText = StoryFormat.encodeStory(summarizer.assemble(inputs));
	    	
	    	saveContent();
	    }
	    	
		private void saveContent() {

			int status = AlgorithmConnector.getStatus(pool, topicId);
			AlgorithmConnector.setContentAndStatus(pool, topicId, outputText, status - 1);
			System.out.println("New summary created.");

			if(status == 2){
				SequenceAssembler sa = new SequenceAssembler(topicId, pool);
				sa.start();
			}
		}
	 

	}
}
