package com.storyport.backend;
/**
 * @author Ky�sti
 * An interface for the algorithm that combines fragments. Makes changing the algorithm easier.
 */
public interface MergingAlgorithm {

	
	public abstract Iterable<FragmentPart> assemble(Fragment[] inputs); 
}
