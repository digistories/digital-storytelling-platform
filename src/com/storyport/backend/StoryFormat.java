package com.storyport.backend;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Use this class to transform generated story into a form where the fragment-id precedes the text that is taken from the fragment,
 * and vice versa to get the individual fragment-parts with fragment ids after retrieving
 * @author Pekka
 * TODO: Do something so that users can't break everything by inserting their own id-tags into stories.
 */
public class StoryFormat {
	
	/**
	 * These surround the fragment-ids in the content.
	 * 
	 * So currently the format is like this:
	 * "<<329>>This is part of a fragment that has id 329.<<1312>>This is from another fragment."
	 */
	public static final String ID_START = "<<", ID_END = ">>";
	
	/**
	 * To finish generating a story, use this to encode it to correct format.
	 */
	public static String encodeStory(Iterable<FragmentPart> parts) {
		String story = "";
		for(FragmentPart part : parts) {
			story += ID_START + part.getFragmentId() + ID_END + part.getContent();
		}
		return story;
	}
	
	/**
	 * Use this to parse the FragmentParts from a story that uses the correct format.
	 */
	public static ArrayList<FragmentPart> decodeStory(String story) {
		Pattern p = Pattern.compile(ID_START + "(?<id>\\d+)" + ID_END);
		Matcher m = p.matcher(story);
		
		ArrayList<FragmentPart> parts = new ArrayList<FragmentPart>();
		
		int end = 0;
		int id = -1;
		
		while(m.find()){
			
			if(m.start() > end && id >= 0){
				String s = story.substring(end, m.start());
				parts.add(new FragmentPart(id, s));
			}

			id = Integer.parseInt(m.group("id"));
			end = m.end();
		}
		
		if(end < story.length() && id >= 0){
			String s = story.substring(end, story.length());
			parts.add(new FragmentPart(id, s));
		}
		
		return parts;
	}
}
