package com.storyport.backend;

/**
 * This couples part of a fragment that is used in a generated story to the id of the original fragment.
 * @author Pekka
 *
 */
public class FragmentPart {
	
	private int fragmentId;
	private String content;
	
	/**
	 * @param fragmentId Id of the original fragment that includes this piece of story
	 * @param content Part of the original fragment that is used in the story
	 */
	public FragmentPart(int fragmentId, String content){
		this.fragmentId = fragmentId;
		this.content = content;
	}

	public int getFragmentId() {
		return fragmentId;
	}

	public String getContent() {
		return content;
	}
	
	@Override
	public String toString() {
		return content;
	}
	
}
