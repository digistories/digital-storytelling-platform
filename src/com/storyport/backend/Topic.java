package com.storyport.backend;

public class Topic {

	private int id;
	private String title;
	private String content;
	private String description;
	private String authorId;
	private int status;
	private boolean privateTopic;
	
	public Topic(int id, String title, String content, String description,
			String authorId, int status, boolean privateTopic) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.description = description;
		this.authorId = authorId;
		this.status = status;
		this.privateTopic = privateTopic;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getDescription() {
		return description;
	}

	public String getAuthorId() {
		return authorId;
	}

	public int getStatus() {
		return status;
	}

	public boolean isPrivateTopic() {
		return privateTopic;
	}
	
	@Override
	public String toString() {
		return title;
	}
}
