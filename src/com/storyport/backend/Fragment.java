package com.storyport.backend;

public class Fragment {
	
	private int id;
	private String content;
	private int topicId;
	private String userId;
	private boolean draft;
	private int score;
	
	public Fragment(int id, String content, int topicId, String userId, boolean draft, int score) {
		this.id = id;
		this.content = content;
		this.topicId = topicId;
		this.userId = userId;
		this.draft = draft;
		this.score = score;
	}

	public int getId() {
		return id;
	}
	public String getContent() {
		return content;
	}
	public int getTopicId() {
		return topicId;
	}
	public String getUserId() {
		return userId;
	}
	public boolean isDraft() {
		return draft;
	}
	public int getScore() {
		return score;
	}
}
