package com.storyport.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;

/**
 * UI to backend connector. Something needs to be done, this class is ridiculous. 
 * @author Ky�sti
 *
 */
public class FrontendConnector {
	
	public static boolean isAdmin(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		boolean isAdmin = false;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT * FROM Admins WHERE user_id=?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        if(!resultSet.isClosed()){
	        	isAdmin = true;
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return isAdmin;
	}
	
	public static int getUsersKarma(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		int karma = 0;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT SUM(score) FROM Episode WHERE user_id = ?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        karma = resultSet.getInt(1);
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return karma;
	}
	
	public static int getUsersTopicCount(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		int topics = 0;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT COUNT(*) FROM Topic WHERE author = ?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        topics = resultSet.getInt(1);
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return topics;
	}
	
	public static int getUsersFragmentCount(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		int topics = 0;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT COUNT(*) FROM Episode WHERE user_id = ?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        topics = resultSet.getInt(1);
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return topics;
	}
	
	/**
	 * Try to vote an episode, fails if the user has voted before
	 * 
	 * @param upvote true = +1 for the episode, false = -1
	 * @return true if vote is succesful, i. e. has not voted before
	 */
	public static boolean tryToVote(JDBCConnectionPool pool, int fragmentId, String userId, boolean upvote){
		Connection connection = null;  
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		boolean hasVoted = false;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT * FROM Vote WHERE episode_id=? AND user_id=?");
	        statement.setInt(1, fragmentId );
	        statement.setString(2, userId);
	        
	        resultSet = statement.executeQuery();
	        if(!resultSet.isClosed()){
	        	hasVoted = true;
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(!hasVoted){
			voteFragment(pool, fragmentId, userId, upvote);
			return true;
		}
		return false;
			
	}
	
	public static void voteFragment(JDBCConnectionPool pool, int fragmentId, String userId, boolean upvote){
		Connection connection = null;  
		PreparedStatement statement = null, statement2 = null;

		try{
	        connection = pool.reserveConnection();
	        
	        if(upvote)
	        	statement = connection.prepareStatement("UPDATE Episode SET score = score + 1 WHERE id = ?");
	        else
	        	statement = connection.prepareStatement("UPDATE Episode SET score = score - 1 WHERE id = ?");
	        
	        statement.setInt(1, fragmentId);
	        statement.executeUpdate();
	        
	        statement2 = connection.prepareStatement("INSERT INTO Vote (episode_id, user_id) VALUES(?,?)");
	        statement2.setInt(1, fragmentId);
	        statement2.setString(2, userId);
	        statement2.executeUpdate();
	        
	        connection.commit();

		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
				statement2.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static void addPermission(JDBCConnectionPool pool, int topicId, String userId){
		Connection connection = null;  
		PreparedStatement statement = null;

		try{
	        connection = pool.reserveConnection();
	        
	        /* ADD INFO */
	        statement = connection.prepareStatement("INSERT INTO Permissions (user_id,topic_id) VALUES(?,?)");
	        
	        statement.setString(1, userId);
	        statement.setInt(2, topicId);
	        statement.executeUpdate();
	        
	        connection.commit();

		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static boolean hasPermission(JDBCConnectionPool pool, int topicId, String userId, boolean notNull){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		boolean hasPermission = false;
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        if(userId != null){
		        statement = connection.prepareStatement("SELECT id FROM Topic WHERE id=? AND (private=0 OR author=?) " + (notNull ? "AND content IS NOT NULL" : "") +
		        		" UNION SELECT topic_id FROM Permissions INNER JOIN Topic ON Topic.id = Permissions.topic_id AND Topic.id = ? AND Permissions.user_id = ? " + (notNull ? "AND content IS NOT NULL" : ""));
		        
		        statement.setInt(1, topicId );
		        statement.setString(2, userId );
		        statement.setInt(3, topicId );
		        statement.setString(4, userId );
		        
	        }
	        else{
	        	statement = connection.prepareStatement("SELECT * FROM Topic WHERE id=? AND (private=0 " + (notNull ? "AND content IS NOT NULL" : "") +  ")");
	        	
	        	statement.setInt(1, topicId );
	        }
	        
	        resultSet = statement.executeQuery();
	        hasPermission = !resultSet.isClosed();
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return hasPermission;
	}
	
	public static void deleteFragment(JDBCConnectionPool pool, Fragment fragment){
		Connection connection = null;  
		PreparedStatement statement = null;
		
		try{
			
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("DELETE FROM Episode WHERE id=?");
	        statement.setInt(1, fragment.getId() );
	        
	        statement.executeUpdate();
	        connection.commit();
	        
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		StoryBuilder.askForBuild(pool, fragment.getTopicId());
	}
	
	public static void deleteTopic(JDBCConnectionPool pool, int topicId){
		Connection connection = null;  
		PreparedStatement statement1 = null;
		PreparedStatement statement2 = null;
		
		try{
			
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement1 = connection.prepareStatement("DELETE FROM Episode WHERE topic_id=?");
	        statement1.setInt(1, topicId );
	        
	        statement1.executeUpdate();
	        
	        statement2 = connection.prepareStatement("DELETE FROM Topic WHERE id=?");
	        statement2.setInt(1, topicId );
	        
	        statement2.executeUpdate();
	        
	        connection.commit();
	        
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement1.close();
				statement2.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static Fragment[] getUsersFragments(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		ArrayList<Fragment> fragmentArray = new ArrayList<Fragment>();
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT id,content,topic_id,user_id,draft,score FROM Episode WHERE user_id=?");
	        statement.setString(1, userId );
	        
	        resultSet = statement.executeQuery();
	        while(resultSet.next()){
	        	fragmentArray.add(new Fragment(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4),resultSet.getInt(5)==1,resultSet.getInt(6) ));
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		Fragment[] fragments = new Fragment[fragmentArray.size()];
		fragmentArray.toArray(fragments);
		
    	return fragments;
	}
	
	public static void addFragment(JDBCConnectionPool pool, String userId, int id, String content){
		Connection connection = null;  
		PreparedStatement statement = null;

		try{
	        connection = pool.reserveConnection();
	        
	        /* ADD INFO */
	        statement = connection.prepareStatement("INSERT INTO Episode (user_id,content,topic_id,score) VALUES(?,?,?,0)");
	        
	        statement.setString(1, userId);
	        statement.setString(2, content);
	        statement.setInt(3, id);
	        statement.executeUpdate();
	        
	        connection.commit();

	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		StoryBuilder.askForBuild(pool, id);
	}
	
	public static Topic[] getUsersWrittenTopics(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		ArrayList<Topic> topicArray = new ArrayList<Topic>();
		
		try{
	        connection = pool.reserveConnection();

	        statement = connection.prepareStatement("SELECT DISTINCT topic_id,title,Topic.content,description,author,status,private FROM Episode INNER JOIN Topic ON Topic.id = Episode.topic_id AND Episode.user_id = ?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        while(resultSet.next()){
	        	topicArray.add(new Topic(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), 
	        			resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(5)==1));
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		Topic[] topicss = new Topic[topicArray.size()];
		topicArray.toArray(topicss);
		
    	return topicss;
	}
	
	public static Topic[] getUsersTopics(JDBCConnectionPool pool, String userId){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		ArrayList<Topic> topicArray = new ArrayList<Topic>();
		
		try{
			
	        connection = pool.reserveConnection();
	        

	        statement = connection.prepareStatement("SELECT id,title,content,description,author,status,private FROM Topic WHERE author=?");
	        statement.setString(1, userId);
	        
	        resultSet = statement.executeQuery();
	        while(resultSet.next()){
	        	topicArray.add(new Topic(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), 
	        			resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(5)==1));
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		Topic[] topicss = new Topic[topicArray.size()];
		topicArray.toArray(topicss);
		
    	return topicss;
	}
	
	public static Topic[] getAvailableTopics(JDBCConnectionPool pool, String profileId, boolean notNull){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		ArrayList<Topic> topicArray = new ArrayList<Topic>();
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT id,title,content,description,author,status,private FROM Topic WHERE (private=0 OR author=?) " + (notNull ? "AND content IS NOT NULL" : "") +
	        		" UNION SELECT topic_id,title,content,description,author,status,private FROM Permissions INNER JOIN Topic ON Topic.id = Permissions.topic_id AND Permissions.user_id = ? " + (notNull ? "AND content IS NOT NULL" : ""));
	        
	        statement.setString(1, profileId );
	        statement.setString(2, profileId );
	        
	        resultSet = statement.executeQuery();
	        while(resultSet.next()){
	        	topicArray.add(new Topic(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), 
	        			resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(5)==1));
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		Topic[] topicss = new Topic[topicArray.size()];
		topicArray.toArray(topicss);
		
    	return topicss;
	}

	public static int[] getAvailableTopicIds(JDBCConnectionPool pool, String profileId, boolean notNull){
		
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		int[] topics = null; 
		try{
			
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        if(profileId != null){
		        statement = connection.prepareStatement("SELECT id FROM Topic WHERE (private=0 OR author=?) " + (notNull ? "AND content IS NOT NULL" : "") +
		        		" UNION SELECT topic_id FROM Permissions INNER JOIN Topic ON Topic.id = Permissions.topic_id AND Permissions.user_id = ? " + (notNull ? "AND content IS NOT NULL" : ""));
		        
		        statement.setString(1, profileId );
		        statement.setString(2, profileId );
	        }
	        else{
	        	statement = connection.prepareStatement("SELECT id FROM Topic WHERE (private=0 " + (notNull ? "AND content IS NOT NULL" : "") +  ")");
	        }
	        resultSet = statement.executeQuery();
	        
	        ArrayList<Integer> topicsList = new ArrayList<Integer>();
	        while(resultSet.next()){
	        	topicsList.add(resultSet.getInt(1));
	        }
	        topics = new int[topicsList.size()];
	        for(int i = 0; i < topics.length; i++){
	        	topics[i] = topicsList.get(i);
	        }
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return topics;
	}

	public static Fragment getFragment(JDBCConnectionPool pool, int id){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		Fragment fragment = null;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT id,content,topic_id,user_id,draft,score FROM Episode WHERE id=?");
	        statement.setInt(1, id );
	        
	        resultSet = statement.executeQuery();
	        if(!resultSet.isClosed()){
	        	fragment = new Fragment(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4),resultSet.getInt(5)==1,resultSet.getInt(6) );
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
    	return fragment;
	}
	
	public static Topic getTopic(JDBCConnectionPool pool, int id){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		Topic topic = null;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT id,title,content,description,author,status,private FROM Topic WHERE id=?");
	        statement.setInt(1, id );
	        
	        resultSet = statement.executeQuery();
	        if(!resultSet.isClosed()){
	        	topic = new Topic(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4), 
	        			resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(5)==1);
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
    	return topic;
	}
	
	public static int getTopicFragmentCount(JDBCConnectionPool pool, int id){
		Connection connection = null;  
	    ResultSet resultSet = null;  
		PreparedStatement statement = null;
		
		int count = 0;
		
		try{
	        connection = pool.reserveConnection();
	        
	        /* GET INFO*/
	        statement = connection.prepareStatement("SELECT COUNT(*) FROM Episode WHERE topic_id=?");
	        statement.setInt(1, id );
	        
	        resultSet = statement.executeQuery();
	        if(!resultSet.isClosed()){
	        	count = resultSet.getInt(1);
	        }
	        
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				if(!resultSet.isClosed())
					resultSet.close();
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
    	return count;
	}

	public static void addTopic(JDBCConnectionPool pool, String title, String userId, boolean privateTopic, String descrition){
		Connection connection = null;  
		PreparedStatement statement = null;

		try{
	        connection = pool.reserveConnection();
	        
	        /* ADD INFO */
	        statement = connection.prepareStatement("INSERT INTO Topic (title,status,author,private,description) VALUES(?,0,?,?,?)");
	        
	        statement.setString(1, title);
	        statement.setString(2, userId);
	        statement.setInt(3, privateTopic? 1 : 0);
	        statement.setString(4, descrition);
	        
	        statement.executeUpdate();
	        
	        connection.commit();

		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
	        	connection.close();
	        	pool.releaseConnection(connection);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
