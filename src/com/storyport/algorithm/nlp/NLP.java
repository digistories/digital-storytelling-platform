package com.storyport.algorithm.nlp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class NLP {

	Properties props;
    StanfordCoreNLP pipeline;
    
    public NLP(){
        this.props = new Properties();
        this.props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse");
        this.pipeline = new StanfordCoreNLP(this.props);
    }
    
    public String[] getSynonyms(String word){
        System.setProperty("wordnet.database.dir", "/storyport/dict");
        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(word);
        List<String> AllResults = new ArrayList<String>();

        for (int i = 0; i < synsets.length; i++)
        {
            String[] wordForms = synsets[i].getWordForms();
            for (int j = 0; j < wordForms.length; j++)
            {
                if (!wordForms[j].equalsIgnoreCase(word))
                {
                    AllResults.add(wordForms[j]);
                }
            }
        }
        
        //Adding the original word for once
        AllResults.add(word);

        String[] convertedArr = AllResults.toArray(new String[0]);
        Set<String> temp = new HashSet<String>(Arrays.asList(convertedArr));
        String[] DistincResults = temp.toArray(new String[0]);


        return  DistincResults;
    }
    
    public List<CoreMap> getSentences(String str){
        
        String text = normalizeString(str);
        List<CoreMap> sentences = new ArrayList<CoreMap>();
        
        try {
            Annotation document  = new Annotation(text);
            this.pipeline.annotate(document);

            sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        }
        catch(Exception e)
        {
        
        }
        return sentences;
    }
    
    public String normalizeString(String text)
    {
        return text.replaceAll("\\.(?!\\s)", ". ");
    }
}
