package com.storyport.algorithm.nlp;
/**
 * @author Ky�sti
 */

import java.util.ArrayList;
import java.util.List;

import com.storyport.algorithm.NLPInterface;
import com.storyport.algorithm.Sentence;
import com.storyport.algorithm.Stemmer;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.CoreMap;


public class AlgorithmNLP implements NLPInterface{
		
	public List<Sentence> getSentences(String input){
		NLP helper = new NLP();
		List<CoreMap> sentences = helper.getSentences(input);
		
		List<Sentence> sentenceObjects = new ArrayList<Sentence>();
        
        Stemmer stem = new Stemmer();
        
        for(CoreMap sentence: sentences) {
            
        	ArrayList<String> keywords = new ArrayList<String>();
            
        	for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {

                // this is the text of the token
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                // this is the POS tag of the token
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                // this is the NER label of the token
                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);  
                //System.out.printf("%s\n%s\n%s\n\n\n", word, pos, ne);
                          
                //if (pos.equals("NNP") || pos.equals("NN") || pos.equals("NNS") || pos.equals("NNPS") || pos.equals("CD") && !ne.equals("PERSON")){
                if (pos.equals("NNP") || pos.equals("NN") || pos.equals("NNS") || pos.equals("NNPS") ||
                        pos.equals("CD") || pos.equals("VB")  || ne.equals("PERSON") || pos.equals("VBD")  || 
                        pos.equals("VBG")  || pos.equals("VBN")  ){

                    keywords.add(helper.getSynonyms(stem.stem(word))[0]);
                }
            }
        	
        	sentenceObjects.add(new Sentence(sentence.toString(), keywords));
        }
        
        return sentenceObjects;
        
	}	
}
