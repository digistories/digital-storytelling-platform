package com.storyport.algorithm;

/**
 * Represents a single unique keyword that has a score.
 * @author Pekka
 */
public class KeyWord implements Comparable<KeyWord> {
	
	private String word;
	private float score;
	
	public KeyWord(String word, float score){
		this.word = word;
		this.score = score;
	}

	@Override
	public int compareTo(KeyWord other) {
		if(Math.abs(score - other.getScore()) < 0.001f)
			return 0;
		
		return score > other.getScore() ? 1 : -1;
	}
	
	public boolean equals(String s){
		return s.equals(word);
	}
	
	public void addScore(float add){
		score += add;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}
}
