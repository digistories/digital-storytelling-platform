package com.storyport.algorithm;

import java.util.ArrayList;
import java.util.List;

import com.storyport.algorithm.nlp.AlgorithmNLP;
import com.storyport.backend.Fragment;
import com.storyport.backend.FragmentPart;
import com.storyport.backend.MergingAlgorithm;

/**
 * Assembles summaries from fragments.
 * @author Pekka
 */
public class Summarizer implements MergingAlgorithm{
	
	private Fragment[] inputs;
	private List<List<Sentence>> allSentences;
	private KeyWordData kwData;
	
	private FragmentPartData bestPart;
	
	private ArrayList<FragmentPart> fragmentParts;

	public ArrayList<FragmentPart> assemble(Fragment[] inputs){
		this.inputs = inputs;
		initialize();
		return findBestParts();
	}
	
	public void initialize(){
		allSentences = new ArrayList<List<Sentence>>();
		
		// calculate keyword-scores and init sentences
		for(int i = 0; i < inputs.length; i++){
			List<Sentence> fragmentSentences = new AlgorithmNLP().getSentences(inputs[i].getContent());
			allSentences.add(fragmentSentences);
		}
		kwData = new KeyWordData(allSentences);
		
		fragmentParts = new ArrayList<FragmentPart>();
	}
	
	private ArrayList<FragmentPart> findBestParts(){
		do{
			bestPart = null;
			
			for(int i = 0; i < allSentences.size(); i++){ // for each fragment
				
				FragmentPartData bestPartOfFragment = FragmentEvaluator.getBestPartOfFragment(allSentences, i, kwData);
				
				Fragment fragment = inputs[i];
				fragment.getScore(); // TODO: ADD/REMOVE POINTS BASED ON FRAGMENT SCORE
				
				if(bestPartOfFragment != null && bestPartOfFragment.isBetterThan(bestPart)){
					bestPart = bestPartOfFragment;
				}
			}
			
			if(bestPart != null){
				List<Sentence> bestPartSentences = popSentences(bestPart);
				
				kwData.updateScores(bestPartSentences);
				
				fragmentParts.add(new FragmentPart(inputs[bestPart.getFragmentIndex()].getId(), catenateSentences(bestPartSentences)));
			}
		}
		while(shouldFindNewParts());
		
		return fragmentParts;
	}
	
	/**
	 * End conditions for the loop.
	 */
	private boolean shouldFindNewParts(){
		return fragmentParts.size() < inputs.length * 2
				&& bestPart != null
				&& bestPart.getScore() > 0;
	}
	
	/**
	 * Sets the chosen sentences null in allSentences so they won't be selected again and returns these sentences.
	 */
	public List<Sentence> popSentences(FragmentPartData chosen){
		List<Sentence> sentences = new ArrayList<Sentence>();
		for(int i = chosen.getFirstSentenceIndex(); i < chosen.getFirstSentenceIndex() + chosen.getAmountOfSentences(); i++){
			sentences.add(allSentences.get(chosen.getFragmentIndex()).get(i));
			allSentences.get(chosen.getFragmentIndex()).set(i, null);
		}
		return sentences;
	}
	
	private static String catenateSentences(List<Sentence> sentences){
		String s = "";
		for(int i = 0; i < sentences.size(); i++){
			s += sentences.get(i).toString();
			if(i != sentences.size()-1)
				s += " ";
		}
		return s;
	}
}
