package com.storyport.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Container for keywords.
 * @author Pekka
 */
public class KeyWordData {
	
	private ArrayList<KeyWord> keyWords;
	
	public KeyWordData(List<List<Sentence>> sentences){
		keyWords = new ArrayList<KeyWord>();
		for(List<Sentence> sList : sentences){
			addKeyWords(sList);
		}
		Collections.sort(keyWords);
	}
	
	public int getRank(String word){
		int rank = 1;
		for(int i = 0; i < keyWords.size(); i++){
			
			// if multiple keywords have the same score, the rank is based on the lowest index
			if(i > 0 && keyWords.get(i).compareTo(keyWords.get(i-1)) != 0)
				rank = i+1;
			
			if(keyWords.get(i).equals(word))
				return rank;
		}
		return -1;
	}
	
	public int getAmountOfKeyWords(){
		return keyWords.size();
	}
	
	/**
	 * Decreases scores of those words that have been selected, and sorts keywords again.
	 */
	public void updateScores(List<Sentence> selectedPart){
		for(Sentence sentence : selectedPart){
			for(String word : sentence.getKeywords()){
				decreaseScore(word);
			}
		}
		Collections.sort(keyWords);
	}
	
	/**
	 * Halves the score of the given word.
	 */
	private void decreaseScore(String word){
		for(KeyWord kw : keyWords){
			if(kw.equals(word)){
				kw.setScore(kw.getScore() / 2f);
				return;
			}
		}
	}

	/**
	 * Increments keyword scores by the words that are in fragmentSentences.
	 */
	private void addKeyWords(List<Sentence> fragmentSentences){
		
		// 1. count the amount of each keyword in this fragment
		HashMap<String, Float> fragmentKeyWords = new HashMap<String, Float>();
		for(Sentence sentence : fragmentSentences){
			for(String s : sentence.getKeywords()){
				
				Float amount = fragmentKeyWords.get(s);
				
				if(amount == null){
					fragmentKeyWords.put(s, 1f);
				}
				else{
					fragmentKeyWords.replace(s, amount+1f);
				}
			}
		}
		
		// 2. reduce how much multiple keywords in the same fragment affect the score
		for(String s : fragmentKeyWords.keySet()){
			float value = fragmentKeyWords.get(s);
			
			float newValue = (float) Math.pow(value, 0.5); // square root
			if(newValue > 3f) newValue = 3f; // max score from one fragment
			
			fragmentKeyWords.replace(s, newValue);
		}
		
		// 3. add the adjusted scores to keywords
		for(String word : fragmentKeyWords.keySet()){
			boolean found = false;
			for(KeyWord keyWord : keyWords){
				if(keyWord.equals(word)){
					keyWord.addScore(fragmentKeyWords.get(word));
					found = true;
					break;
				}
			}
			if(!found)
				keyWords.add(new KeyWord(word, fragmentKeyWords.get(word)));
		}
	}
}
