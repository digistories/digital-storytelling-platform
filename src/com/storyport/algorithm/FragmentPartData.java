package com.storyport.algorithm;

/**
 * Contains information about a group of sentences:
 * - where to find the actual sentences
 * - score
 * @author Pekka
 */
public class FragmentPartData {
	
	// location of this part in the 2-dimensional sentences-list
	private int fragmentIndex;
	private int firstSentenceIndex;
	private int amountOfSentences;
	
	private float score;

	public FragmentPartData(int fragmentIndex, int firstSentenceIndex, int amountOfSentences, float score) {
		this.fragmentIndex = fragmentIndex;
		this.firstSentenceIndex = firstSentenceIndex;
		this.amountOfSentences = amountOfSentences;
		this.score = score;
	}
	
	public boolean isBetterThan(FragmentPartData other){
		if(other == null) return true;
		return score > other.getScore();
	}

	public int getFragmentIndex() {
		return fragmentIndex;
	}

	public int getFirstSentenceIndex() {
		return firstSentenceIndex;
	}

	public int getAmountOfSentences() {
		return amountOfSentences;
	}

	public float getScore() {
		return score;
	}
	
}
