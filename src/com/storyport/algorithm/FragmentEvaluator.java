package com.storyport.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pekka
 */
public class FragmentEvaluator {
	
	/**
	 * Returns the score of the best part of one fragment and saves the best part to the parameter bestPartOfFragment.
	 */
	public static FragmentPartData getBestPartOfFragment(List<List<Sentence>> allSentences, int fragmentIndex, KeyWordData kwData){
		
		List<Sentence> fragmentSentences = allSentences.get(fragmentIndex);
		
		FragmentPartData bestPart = null;
		
		for(int i = 0; i < fragmentSentences.size(); i++){
			
			// best group of consecutive sentences starting from this sentence found so far and their score:
			List<Sentence> sentences = new ArrayList<Sentence>();
			float score = -Float.MAX_VALUE;
			
			for(int j = i; j < fragmentSentences.size(); j++){
				if(fragmentSentences.get(j) == null)
					break;
				
				sentences.add(fragmentSentences.get(j));
				float newScore = getScore(sentences, kwData);
				
				if(newScore > score){
					score = newScore;
				}
				else break;
			}
			
			if(bestPart == null || score > bestPart.getScore()){
				bestPart = new FragmentPartData(fragmentIndex, i, sentences.size(), score);
			}
		}
		
		return bestPart;
	}
	
	/**
	 * Calculates the score of one or more consecutive sentences.
	 */
	private static float getScore(List<Sentence> sentences, KeyWordData kwData){
		int kwCount = kwData.getAmountOfKeyWords();
		
		int sumOfRanks = 0;
		int wordCount = 0;
		
		for(Sentence sentence : sentences){
			
			// if we find a sentence with no keywords, this part contains unimportant information
			// and we give this part a bad score
			if(sentence.getKeywords().size() < 1){
				return -100f; // TODO: check the value
			}
			
			wordCount += sentence.getKeywords().size();
			for(String word : sentence.getKeywords()){
				sumOfRanks += kwData.getRank(word);
			}
		}
		
		float averageRank = (0f + sumOfRanks) / wordCount;
		float score = (averageRank / kwCount) - 0.6f; // TODO: check the value
		
		// TODO: give points for having multiple sentences
		
		return score;
	}
	
}
