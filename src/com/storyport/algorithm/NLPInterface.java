package com.storyport.algorithm;

import java.util.List;

public interface NLPInterface {

	/**
	 * This interface connects the algorithm to a NLP library. 
	 * It takes a fragment's text as an input and returns a list of Sentence-objects.
	 * Sentence objects should contain a single sentence and it's keywords. 
	 * @param input a fragment's content
	 * @return a list of Sentence-objects in the fragment. 
	 */
	public List<Sentence> getSentences(String input);
}
