package com.storyport.algorithm;

import java.util.List;

public class Sentence {

	private String text;
	private List<String> keywords;
	
	public Sentence(String text, List<String> keywords){
		this.text = text;
		this.keywords = keywords;
	}
	
	@Override
	public String toString(){
		return text;
	}
	
	public List<String> getKeywords(){
		return keywords;
	}
}
