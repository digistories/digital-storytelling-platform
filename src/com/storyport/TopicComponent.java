package com.storyport;

import com.storyport.backend.FrontendConnector;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

public class TopicComponent extends CssLayout {
	
	private static final int TITLE_MIN_LENGTH = 2;
	private static final int TITLE_MAX_LENGTH = 20;
	
	private static final int DESC_MIN_LENGTH = 5;
	private static final int DESC_MAX_LENGTH = 200;
	
	private StringLengthValidator titleValidator;
	private StringLengthValidator descValidator;
	
	public TopicComponent(JDBCConnectionPool pool, String userId){
		this.setPrimaryStyleName("topic-read-panel");
		
		TextField title = new TextField("Title");
		title.setMaxLength(TITLE_MAX_LENGTH);
		titleValidator = new StringLengthValidator("The title must be " + TITLE_MIN_LENGTH + " - " + TITLE_MAX_LENGTH + " characters long.", 
				TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, false);
		
		TextArea description = new TextArea("Description");
		description.setMaxLength(DESC_MAX_LENGTH);
		descValidator = new StringLengthValidator("The description must be " + DESC_MIN_LENGTH + " - " + DESC_MAX_LENGTH + " characters long.", 
				DESC_MIN_LENGTH, DESC_MAX_LENGTH, false);
		
		CheckBox privateTopic = new CheckBox("Private");

		Button createTopic = new Button("Create Topic");
		createTopic.setPrimaryStyleName("theme-button");
		
		addComponent(title);
		addComponent(description);
		//addComponent(privateTopic);
		addComponent(createTopic);
		
		createTopic.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				try{
					titleValidator.validate(title.getValue());
					descValidator.validate(description.getValue());
					
					FrontendConnector.addTopic(pool, title.getValue(), userId, privateTopic.getValue(), description.getValue());
					Page.getCurrent().reload();
					UI.getCurrent().getNavigator().navigateTo("created");
				}
				catch(InvalidValueException ive){
					Notification.show(ive.getMessage(), Notification.Type.HUMANIZED_MESSAGE);
				}
			}
		});
	}
}
