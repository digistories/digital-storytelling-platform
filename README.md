# Setting up the project #

## 1. Install JDK ##
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Add Java path to PATH environment variable, for example C:\Program Files\Java\jdk1.8.0_111\bin

## 2. Install Eclipse ##
Eclipse IDE for Java EE Developers

http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon1a

## 3. Install Vaadin plugin for Eclipse ##
In Eclipse, go Help -> Eclipse Marketplace, search for Vaadin and install the plugin.

## 4. Install Apache Tomcat server for running the project ##
Use version 7.0 to avoid problems:

http://tomcat.apache.org/download-70.cgi

## 5. Install Git ##
https://git-scm.com/downloads

## 6. Install SourceTree if you want to use Git with graphical user interface ##
https://www.sourcetreeapp.com/

## 7. Clone project repository from BitBucket ##
In SourceTree, Clone/New -> Clone Repository

Source Path / URL: copy from the top right corner of this page

Destination Path: new empty folder in your Eclipse workspace that MUST be named DigitalStorytellingPlatform, or shit will happen

## 8. Import your just cloned local project into Eclipse ##
File -> Import -> General -> Existing Projects into Workspace

## 9. Download stuff needed for NLP and database##
Install WordNet 2.1: https://wordnet.princeton.edu/wordnet/download/current-version/

Download jaws-bin.jar: https://web.archive.org/web/20160422035129/http://lyle.smu.edu/~tspell/jaws/jaws-bin.jar

Download SQLite database connector sqlite-jdbc-3.16.1.jar:
https://bitbucket.org/xerial/sqlite-jdbc/downloads/

Download Stanford CoreNLP version 3.3.1:
https://stanfordnlp.github.io/CoreNLP/history.html

Add jaws-bin.jar, sqlite-jdbc-3.16.1.jar and stanford-corenlp-3.3.1-models.jar to WebContent/WEB-INF/lib

## 10. Set up locations of database and dictionary ##
Change line 29 of com.storyport.algorithm.NLP.java to the dict folder of your WordNet installation.
By default it is "C:\\Program Files (x86)\\WordNet\\2.1\\dict"

Change line 49 of com.storyport.StoryPortUI to the location of the SQLite database file DigitalStoryTelling.db

## 11. Set up the Tomcat server ##
In Eclipse's Servers-tab, add your Tomcat 7.0 server, then right-click it, select add/remove and add your DigitalStorytellingPlatform to the server

Start the server and go to http://localhost:8080/DigitalStorytellingPlatform/ to test the application